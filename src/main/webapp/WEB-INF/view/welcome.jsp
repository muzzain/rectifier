<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sample Test cases</title>
<style type="text/css">
table, th, td {
  border: 1px solid black;
}
table {
  width: 100%;
}

th {
  height: 50px;
}

.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
</head>

<body>
	Current Configured AXL End point is
	<b> ${endpoint}</b>
	<br /> Current Configured AXL username is
	<b> ${user}</b>
	<br />
	<button id="execute" type="button">Execute Cases!</button>
	<br />
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script>
		jQuery("#execute").click(function() {
			 console.log("Execute button clicked");
			 $("#content").html('');
			 let loader = $("<div />");
			$("#content").append(loader);
			loader.addClass( "loader" );
			 jQuery.post( "/executeSuit", function( data ) {
				

				// console.log(JSON.stringify(data));
				 let newData = data.reduce(reducerForValidateResult , {});
				 console.log(JSON.stringify(newData));
				 
				let resultTable = $("<table />");
				let resultColumns = $("<tr> <th>Result</th> <th>Impact</th> <th>Recommendation</th> <th>&nbsp;</th>  </tr>");
				let testCategories =  Object.keys(newData);		
				testCategories.forEach(value =>{
					let testCategoryName = $("<tr> <th colspan='4'>"+value+"</th> </tr>");
					resultTable.append(testCategoryName);
					resultTable.append(resultColumns.clone());
					let results = newData[value];
					results.forEach(rowData=>{
						let tableDataRow = $("<tr> <td>"+nullToEmptyStr(rowData["result"])+"</td> <td>"+nullToEmptyStr(rowData["impact"])+"</td> <td>"+nullToEmptyStr(rowData["recommendation"])+"</td> <td>"+getRemediationButton(rowData)+"</td>  </tr>");
						resultTable.append(tableDataRow);
					});
				});
				$("#content").html('');
				$("#content").append(resultTable);
				
				});
		});
		function ldapRemediatioHandeler(){
			let tlsPort = prompt("Please enter 636 OR 3269 for TLs port");
			while(tlsPort != "636" && tlsPort != "3269"  ){
				tlsPort = prompt("Please enter 636 OR 3269 for TLs port");
			}
			data = {
					inputValue : tlsPort
			};
			$.post("/ldapRemediation" ,data , response =>{
				alert(response.result);
			});
		}
		function getRemediationButton(data){
			if(data.enableRemediation){
				return "<button onClick='ldapRemediatioHandeler()'>Remediation</button>";
			}else{
				return "";
			}
		}
		function reducerForValidateResult(finalObject , currentObject , currentIndex){
			let prevList = finalObject[currentObject['name']];
			if(prevList){
				prevList.push(currentObject);
				finalObject[currentObject['name']] = prevList;
			}else{
				prevList = [];
				prevList.push(currentObject);
				finalObject[currentObject['name']] = prevList;
			}
			return finalObject;
			
		}
		function nullToEmptyStr(value){
			if (value) {
			   return value;
			}else{
				return "";
			}
		}
		</script>
	<div id="content"></div>
</body>
</html>