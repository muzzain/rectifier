
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XTransportSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XTransportSettings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transportmode" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="transportUrl" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="transportGatewayUrl" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="httpHost" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="httpPort" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XTransportSettings", propOrder = {
    "transportmode",
    "transportUrl",
    "transportGatewayUrl",
    "httpHost",
    "httpPort"
})
public class XTransportSettings {

    protected String transportmode;
    protected String transportUrl;
    protected String transportGatewayUrl;
    protected String httpHost;
    protected String httpPort;

    /**
     * Gets the value of the transportmode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportmode() {
        return transportmode;
    }

    /**
     * Sets the value of the transportmode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportmode(String value) {
        this.transportmode = value;
    }

    /**
     * Gets the value of the transportUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportUrl() {
        return transportUrl;
    }

    /**
     * Sets the value of the transportUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportUrl(String value) {
        this.transportUrl = value;
    }

    /**
     * Gets the value of the transportGatewayUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportGatewayUrl() {
        return transportGatewayUrl;
    }

    /**
     * Sets the value of the transportGatewayUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportGatewayUrl(String value) {
        this.transportGatewayUrl = value;
    }

    /**
     * Gets the value of the httpHost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHttpHost() {
        return httpHost;
    }

    /**
     * Sets the value of the httpHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHttpHost(String value) {
        this.httpHost = value;
    }

    /**
     * Gets the value of the httpPort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHttpPort() {
        return httpPort;
    }

    /**
     * Sets the value of the httpPort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHttpPort(String value) {
        this.httpPort = value;
    }

}
