
package com.cisco.axl.api;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XDeviceMobility complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XDeviceMobility">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subNetDetails">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="ipv4SubNetDetails">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence minOccurs="0">
 *                             &lt;element name="ipv4Subnet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ipv4SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ipv6SubNetDetails">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence minOccurs="0">
 *                             &lt;element name="ipv6Subnet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ipv6SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="members">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.cisco.com/AXL/API/12.5}XCommonMembersExtension">
 *                 &lt;sequence minOccurs="0">
 *                   &lt;element name="member" type="{http://www.cisco.com/AXL/API/12.5}XDevicePoolDeviceMobility" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XDeviceMobility", propOrder = {
    "name",
    "subNetDetails",
    "members"
})
public class XDeviceMobility {

    protected String name;
    protected XDeviceMobility.SubNetDetails subNetDetails;
    protected XDeviceMobility.Members members;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the subNetDetails property.
     * 
     * @return
     *     possible object is
     *     {@link XDeviceMobility.SubNetDetails }
     *     
     */
    public XDeviceMobility.SubNetDetails getSubNetDetails() {
        return subNetDetails;
    }

    /**
     * Sets the value of the subNetDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link XDeviceMobility.SubNetDetails }
     *     
     */
    public void setSubNetDetails(XDeviceMobility.SubNetDetails value) {
        this.subNetDetails = value;
    }

    /**
     * Gets the value of the members property.
     * 
     * @return
     *     possible object is
     *     {@link XDeviceMobility.Members }
     *     
     */
    public XDeviceMobility.Members getMembers() {
        return members;
    }

    /**
     * Sets the value of the members property.
     * 
     * @param value
     *     allowed object is
     *     {@link XDeviceMobility.Members }
     *     
     */
    public void setMembers(XDeviceMobility.Members value) {
        this.members = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}XCommonMembersExtension">
     *       &lt;sequence minOccurs="0">
     *         &lt;element name="member" type="{http://www.cisco.com/AXL/API/12.5}XDevicePoolDeviceMobility" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "member"
    })
    public static class Members
        extends XCommonMembersExtension
    {

        protected List<XDevicePoolDeviceMobility> member;

        /**
         * Gets the value of the member property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the member property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMember().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link XDevicePoolDeviceMobility }
         * 
         * 
         */
        public List<XDevicePoolDeviceMobility> getMember() {
            if (member == null) {
                member = new ArrayList<XDevicePoolDeviceMobility>();
            }
            return this.member;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="ipv4SubNetDetails">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence minOccurs="0">
     *                   &lt;element name="ipv4Subnet" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ipv4SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ipv6SubNetDetails">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence minOccurs="0">
     *                   &lt;element name="ipv6Subnet" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ipv6SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ipv4SubNetDetails",
        "ipv6SubNetDetails"
    })
    public static class SubNetDetails {

        protected XDeviceMobility.SubNetDetails.Ipv4SubNetDetails ipv4SubNetDetails;
        protected XDeviceMobility.SubNetDetails.Ipv6SubNetDetails ipv6SubNetDetails;

        /**
         * Gets the value of the ipv4SubNetDetails property.
         * 
         * @return
         *     possible object is
         *     {@link XDeviceMobility.SubNetDetails.Ipv4SubNetDetails }
         *     
         */
        public XDeviceMobility.SubNetDetails.Ipv4SubNetDetails getIpv4SubNetDetails() {
            return ipv4SubNetDetails;
        }

        /**
         * Sets the value of the ipv4SubNetDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link XDeviceMobility.SubNetDetails.Ipv4SubNetDetails }
         *     
         */
        public void setIpv4SubNetDetails(XDeviceMobility.SubNetDetails.Ipv4SubNetDetails value) {
            this.ipv4SubNetDetails = value;
        }

        /**
         * Gets the value of the ipv6SubNetDetails property.
         * 
         * @return
         *     possible object is
         *     {@link XDeviceMobility.SubNetDetails.Ipv6SubNetDetails }
         *     
         */
        public XDeviceMobility.SubNetDetails.Ipv6SubNetDetails getIpv6SubNetDetails() {
            return ipv6SubNetDetails;
        }

        /**
         * Sets the value of the ipv6SubNetDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link XDeviceMobility.SubNetDetails.Ipv6SubNetDetails }
         *     
         */
        public void setIpv6SubNetDetails(XDeviceMobility.SubNetDetails.Ipv6SubNetDetails value) {
            this.ipv6SubNetDetails = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence minOccurs="0">
         *         &lt;element name="ipv4Subnet" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ipv4SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ipv4Subnet",
            "ipv4SubNetMaskSz"
        })
        public static class Ipv4SubNetDetails {

            protected String ipv4Subnet;
            protected String ipv4SubNetMaskSz;

            /**
             * Gets the value of the ipv4Subnet property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpv4Subnet() {
                return ipv4Subnet;
            }

            /**
             * Sets the value of the ipv4Subnet property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpv4Subnet(String value) {
                this.ipv4Subnet = value;
            }

            /**
             * Gets the value of the ipv4SubNetMaskSz property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpv4SubNetMaskSz() {
                return ipv4SubNetMaskSz;
            }

            /**
             * Sets the value of the ipv4SubNetMaskSz property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpv4SubNetMaskSz(String value) {
                this.ipv4SubNetMaskSz = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence minOccurs="0">
         *         &lt;element name="ipv6Subnet" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ipv6SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ipv6Subnet",
            "ipv6SubNetMaskSz"
        })
        public static class Ipv6SubNetDetails {

            protected String ipv6Subnet;
            protected String ipv6SubNetMaskSz;

            /**
             * Gets the value of the ipv6Subnet property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpv6Subnet() {
                return ipv6Subnet;
            }

            /**
             * Sets the value of the ipv6Subnet property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpv6Subnet(String value) {
                this.ipv6Subnet = value;
            }

            /**
             * Gets the value of the ipv6SubNetMaskSz property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpv6SubNetMaskSz() {
                return ipv6SubNetMaskSz;
            }

            /**
             * Sets the value of the ipv6SubNetMaskSz property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpv6SubNetMaskSz(String value) {
                this.ipv6SubNetMaskSz = value;
            }

        }

    }

}
