
package com.cisco.axl.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UpdateExpresswayCConfigurationReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateExpresswayCConfigurationReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIRequest">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="uuid" type="{http://www.cisco.com/AXL/API/12.5}XUUID"/>
 *           &lt;element name="HostNameOrIP" type="{http://www.cisco.com/AXL/API/12.5}String255"/>
 *         &lt;/choice>
 *         &lt;element name="newHostNameOrIP" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.cisco.com/AXL/API/12.5}String50" minOccurs="0"/>
 *         &lt;element name="X509SubjectNameorSubjectAlternateName" type="{http://www.cisco.com/AXL/API/12.5}String4096" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateExpresswayCConfigurationReq", propOrder = {
    "uuid",
    "hostNameOrIP",
    "newHostNameOrIP",
    "description",
    "x509SubjectNameorSubjectAlternateName"
})
public class UpdateExpresswayCConfigurationReq
    extends APIRequest
{

    protected String uuid;
    @XmlElement(name = "HostNameOrIP")
    protected String hostNameOrIP;
    protected String newHostNameOrIP;
    @XmlElementRef(name = "description", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "X509SubjectNameorSubjectAlternateName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> x509SubjectNameorSubjectAlternateName;

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the hostNameOrIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostNameOrIP() {
        return hostNameOrIP;
    }

    /**
     * Sets the value of the hostNameOrIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostNameOrIP(String value) {
        this.hostNameOrIP = value;
    }

    /**
     * Gets the value of the newHostNameOrIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewHostNameOrIP() {
        return newHostNameOrIP;
    }

    /**
     * Sets the value of the newHostNameOrIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewHostNameOrIP(String value) {
        this.newHostNameOrIP = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the x509SubjectNameorSubjectAlternateName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getX509SubjectNameorSubjectAlternateName() {
        return x509SubjectNameorSubjectAlternateName;
    }

    /**
     * Sets the value of the x509SubjectNameorSubjectAlternateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setX509SubjectNameorSubjectAlternateName(JAXBElement<String> value) {
        this.x509SubjectNameorSubjectAlternateName = value;
    }

}
