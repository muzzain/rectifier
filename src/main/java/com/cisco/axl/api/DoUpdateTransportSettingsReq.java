
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * This allows to change or modify the transport settings
 * 
 * <p>Java class for DoUpdateTransportSettingsReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DoUpdateTransportSettingsReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transportMode" type="{http://www.cisco.com/AXL/API/12.5}String255"/>
 *         &lt;element name="url" type="{http://www.cisco.com/AXL/API/12.5}String255"/>
 *         &lt;element name="httpHost" type="{http://www.cisco.com/AXL/API/12.5}String255"/>
 *         &lt;element name="httpPort" type="{http://www.cisco.com/AXL/API/12.5}String255"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DoUpdateTransportSettingsReq", propOrder = {
    "transportMode",
    "url",
    "httpHost",
    "httpPort"
})
public class DoUpdateTransportSettingsReq {

    @XmlElement(required = true)
    protected String transportMode;
    @XmlElement(required = true)
    protected String url;
    @XmlElement(required = true)
    protected String httpHost;
    @XmlElement(required = true)
    protected String httpPort;

    /**
     * Gets the value of the transportMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportMode() {
        return transportMode;
    }

    /**
     * Sets the value of the transportMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportMode(String value) {
        this.transportMode = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the httpHost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHttpHost() {
        return httpHost;
    }

    /**
     * Sets the value of the httpHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHttpHost(String value) {
        this.httpHost = value;
    }

    /**
     * Gets the value of the httpPort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHttpPort() {
        return httpPort;
    }

    /**
     * Sets the value of the httpPort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHttpPort(String value) {
        this.httpPort = value;
    }

}
