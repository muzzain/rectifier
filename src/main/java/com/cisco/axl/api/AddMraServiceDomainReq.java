
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddMraServiceDomainReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddMraServiceDomainReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIRequest">
 *       &lt;sequence>
 *         &lt;element name="mraServiceDomain" type="{http://www.cisco.com/AXL/API/12.5}XMraServiceDomain"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddMraServiceDomainReq", propOrder = {
    "mraServiceDomain"
})
public class AddMraServiceDomainReq
    extends APIRequest
{

    @XmlElement(required = true)
    protected XMraServiceDomain mraServiceDomain;

    /**
     * Gets the value of the mraServiceDomain property.
     * 
     * @return
     *     possible object is
     *     {@link XMraServiceDomain }
     *     
     */
    public XMraServiceDomain getMraServiceDomain() {
        return mraServiceDomain;
    }

    /**
     * Sets the value of the mraServiceDomain property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMraServiceDomain }
     *     
     */
    public void setMraServiceDomain(XMraServiceDomain value) {
        this.mraServiceDomain = value;
    }

}
