
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetMraServiceDomainRes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetMraServiceDomainRes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIResponse">
 *       &lt;sequence>
 *         &lt;element name="return">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="mraServiceDomain" type="{http://www.cisco.com/AXL/API/12.5}RMraServiceDomain"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetMraServiceDomainRes", propOrder = {
    "_return"
})
public class GetMraServiceDomainRes
    extends APIResponse
{

    @XmlElement(name = "return", required = true)
    protected GetMraServiceDomainRes.Return _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GetMraServiceDomainRes.Return }
     *     
     */
    public GetMraServiceDomainRes.Return getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetMraServiceDomainRes.Return }
     *     
     */
    public void setReturn(GetMraServiceDomainRes.Return value) {
        this._return = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="mraServiceDomain" type="{http://www.cisco.com/AXL/API/12.5}RMraServiceDomain"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mraServiceDomain"
    })
    public static class Return {

        @XmlElement(required = true)
        protected RMraServiceDomain mraServiceDomain;

        /**
         * Gets the value of the mraServiceDomain property.
         * 
         * @return
         *     possible object is
         *     {@link RMraServiceDomain }
         *     
         */
        public RMraServiceDomain getMraServiceDomain() {
            return mraServiceDomain;
        }

        /**
         * Sets the value of the mraServiceDomain property.
         * 
         * @param value
         *     allowed object is
         *     {@link RMraServiceDomain }
         *     
         */
        public void setMraServiceDomain(RMraServiceDomain value) {
            this.mraServiceDomain = value;
        }

    }

}
