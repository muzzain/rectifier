
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LPhoneActivationCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LPhoneActivationCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="activationCode" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="activationCodeExpiry" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
 *         &lt;element name="phoneName" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="phoneDescription" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="phoneModel" type="{http://www.cisco.com/AXL/API/12.5}XModel" minOccurs="0"/>
 *         &lt;element name="enableActivationId" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.cisco.com/AXL/API/12.5}XFkType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="uuid" type="{http://www.cisco.com/AXL/API/12.5}XUUID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LPhoneActivationCode", propOrder = {
    "activationCode",
    "activationCodeExpiry",
    "phoneName",
    "phoneDescription",
    "phoneModel",
    "enableActivationId",
    "userId"
})
public class LPhoneActivationCode {

    protected String activationCode;
    protected String activationCodeExpiry;
    protected String phoneName;
    protected String phoneDescription;
    protected String phoneModel;
    protected String enableActivationId;
    protected XFkType userId;
    @XmlAttribute(name = "uuid")
    protected String uuid;

    /**
     * Gets the value of the activationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * Sets the value of the activationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationCode(String value) {
        this.activationCode = value;
    }

    /**
     * Gets the value of the activationCodeExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationCodeExpiry() {
        return activationCodeExpiry;
    }

    /**
     * Sets the value of the activationCodeExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationCodeExpiry(String value) {
        this.activationCodeExpiry = value;
    }

    /**
     * Gets the value of the phoneName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneName() {
        return phoneName;
    }

    /**
     * Sets the value of the phoneName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneName(String value) {
        this.phoneName = value;
    }

    /**
     * Gets the value of the phoneDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneDescription() {
        return phoneDescription;
    }

    /**
     * Sets the value of the phoneDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneDescription(String value) {
        this.phoneDescription = value;
    }

    /**
     * Gets the value of the phoneModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneModel() {
        return phoneModel;
    }

    /**
     * Sets the value of the phoneModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneModel(String value) {
        this.phoneModel = value;
    }

    /**
     * Gets the value of the enableActivationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnableActivationId() {
        return enableActivationId;
    }

    /**
     * Sets the value of the enableActivationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnableActivationId(String value) {
        this.enableActivationId = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link XFkType }
     *     
     */
    public XFkType getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link XFkType }
     *     
     */
    public void setUserId(XFkType value) {
        this.userId = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
