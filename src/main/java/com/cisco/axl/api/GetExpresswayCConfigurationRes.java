
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetExpresswayCConfigurationRes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetExpresswayCConfigurationRes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIResponse">
 *       &lt;sequence>
 *         &lt;element name="return">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="expresswayCConfiguration" type="{http://www.cisco.com/AXL/API/12.5}RExpresswayCConfiguration"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetExpresswayCConfigurationRes", propOrder = {
    "_return"
})
public class GetExpresswayCConfigurationRes
    extends APIResponse
{

    @XmlElement(name = "return", required = true)
    protected GetExpresswayCConfigurationRes.Return _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GetExpresswayCConfigurationRes.Return }
     *     
     */
    public GetExpresswayCConfigurationRes.Return getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetExpresswayCConfigurationRes.Return }
     *     
     */
    public void setReturn(GetExpresswayCConfigurationRes.Return value) {
        this._return = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="expresswayCConfiguration" type="{http://www.cisco.com/AXL/API/12.5}RExpresswayCConfiguration"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "expresswayCConfiguration"
    })
    public static class Return {

        @XmlElement(required = true)
        protected RExpresswayCConfiguration expresswayCConfiguration;

        /**
         * Gets the value of the expresswayCConfiguration property.
         * 
         * @return
         *     possible object is
         *     {@link RExpresswayCConfiguration }
         *     
         */
        public RExpresswayCConfiguration getExpresswayCConfiguration() {
            return expresswayCConfiguration;
        }

        /**
         * Sets the value of the expresswayCConfiguration property.
         * 
         * @param value
         *     allowed object is
         *     {@link RExpresswayCConfiguration }
         *     
         */
        public void setExpresswayCConfiguration(RExpresswayCConfiguration value) {
            this.expresswayCConfiguration = value;
        }

    }

}
