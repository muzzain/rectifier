package com.cisco.axl.api;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.Detail;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;

import com.cisco.axl.api.GetLdapAuthenticationRes.Return;
import com.cisco.axl.api.ListServiceParameterReq.SearchCriteria;

public class Main {

	static {
		disableSslVerification();
	}

	public static void main(String[] args) throws AXLError_Exception {
		try {
			AXLPort axlClient = getAXLClient();
			GetLdapAuthenticationReq axlParams = new GetLdapAuthenticationReq();
			GetLdapAuthenticationRes ldapAuthentication = axlClient.getLdapAuthentication(axlParams);
			Return resReturn = ldapAuthentication.getReturn();
			System.out.println(resReturn.getLdapAuthentication());
		}catch (SOAPFaultException e) {
			try {
				SOAPFault fault = e.getFault();
				System.out.println(fault.getFaultString());
				Detail detail = fault.getDetail();
				final JAXBContext jaxbc = JAXBContext.newInstance(AXLError.class);
				final Unmarshaller unmarshaller = jaxbc.createUnmarshaller();
				JAXBElement<AXLError> unmarshal = unmarshaller.unmarshal(detail, AXLError.class);
				AXLError value = unmarshal.getValue();
				System.out.println(value.getAxlcode());
			} catch (Exception ew) {
				ew.printStackTrace();
			}
		}
//		ListServiceParameterReq listServiceParameterReq = new ListServiceParameterReq();
//		
//		LServiceParameter returnTag = new LServiceParameter();
//		returnTag.setName("");
//		returnTag.setValue("");
//		
//		listServiceParameterReq.setReturnedTags(returnTag );
//		SearchCriteria criteria = new SearchCriteria();
//		criteria.setProcessNodeName("%");
//		listServiceParameterReq.setSearchCriteria(criteria );
//		
//		ListServiceParameterRes listServiceParameter = axlClient.listServiceParameter(listServiceParameterReq);
//		List<LServiceParameter> serviceParameter = listServiceParameter.getReturn().getServiceParameter();
//		for (LServiceParameter lServiceParameter : serviceParameter) {
//			System.out.println(lServiceParameter.getName() +" => "+ lServiceParameter.getValue());
//			
//		}
	}

	private static AXLPort getAXLClient() {
		AXLAPIService axlService = new AXLAPIService();
		AXLPort axlPort = axlService.getAXLPort();

		((BindingProvider) axlPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				"https://10.10.20.1:8443/axl/");
		((BindingProvider) axlPort).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "administrator");
		((BindingProvider) axlPort).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "ciscopsdt");
		return axlPort;
	}

	private static void disableSslVerification() {
		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}

}
