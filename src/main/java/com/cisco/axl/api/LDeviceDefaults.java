
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LDeviceDefaults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LDeviceDefaults">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="Model" type="{http://www.cisco.com/AXL/API/12.5}XModel" minOccurs="0"/>
 *         &lt;element name="Protocol" type="{http://www.cisco.com/AXL/API/12.5}XDeviceProtocol" minOccurs="0"/>
 *         &lt;element name="LoadInformation" type="{http://www.cisco.com/AXL/API/12.5}XLoadInformation" minOccurs="0"/>
 *         &lt;element name="InactiveLoadInformation" type="{http://www.cisco.com/AXL/API/12.5}XLoadInformation" minOccurs="0"/>
 *         &lt;element name="DevicePoolName" type="{http://www.cisco.com/AXL/API/12.5}XFkType" minOccurs="0"/>
 *         &lt;element name="PhoneButtonTemplate" type="{http://www.cisco.com/AXL/API/12.5}XFkType" minOccurs="0"/>
 *         &lt;element name="PreferActCodeOverAutoReg" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="uuid" type="{http://www.cisco.com/AXL/API/12.5}XUUID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LDeviceDefaults", propOrder = {
    "model",
    "protocol",
    "loadInformation",
    "inactiveLoadInformation",
    "devicePoolName",
    "phoneButtonTemplate",
    "preferActCodeOverAutoReg"
})
public class LDeviceDefaults {

    @XmlElement(name = "Model")
    protected String model;
    @XmlElement(name = "Protocol")
    protected String protocol;
    @XmlElement(name = "LoadInformation")
    protected XLoadInformation loadInformation;
    @XmlElement(name = "InactiveLoadInformation")
    protected XLoadInformation inactiveLoadInformation;
    @XmlElement(name = "DevicePoolName")
    protected XFkType devicePoolName;
    @XmlElement(name = "PhoneButtonTemplate")
    protected XFkType phoneButtonTemplate;
    @XmlElement(name = "PreferActCodeOverAutoReg")
    protected String preferActCodeOverAutoReg;
    @XmlAttribute(name = "uuid")
    protected String uuid;

    /**
     * Gets the value of the model property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModel(String value) {
        this.model = value;
    }

    /**
     * Gets the value of the protocol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * Sets the value of the protocol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocol(String value) {
        this.protocol = value;
    }

    /**
     * Gets the value of the loadInformation property.
     * 
     * @return
     *     possible object is
     *     {@link XLoadInformation }
     *     
     */
    public XLoadInformation getLoadInformation() {
        return loadInformation;
    }

    /**
     * Sets the value of the loadInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link XLoadInformation }
     *     
     */
    public void setLoadInformation(XLoadInformation value) {
        this.loadInformation = value;
    }

    /**
     * Gets the value of the inactiveLoadInformation property.
     * 
     * @return
     *     possible object is
     *     {@link XLoadInformation }
     *     
     */
    public XLoadInformation getInactiveLoadInformation() {
        return inactiveLoadInformation;
    }

    /**
     * Sets the value of the inactiveLoadInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link XLoadInformation }
     *     
     */
    public void setInactiveLoadInformation(XLoadInformation value) {
        this.inactiveLoadInformation = value;
    }

    /**
     * Gets the value of the devicePoolName property.
     * 
     * @return
     *     possible object is
     *     {@link XFkType }
     *     
     */
    public XFkType getDevicePoolName() {
        return devicePoolName;
    }

    /**
     * Sets the value of the devicePoolName property.
     * 
     * @param value
     *     allowed object is
     *     {@link XFkType }
     *     
     */
    public void setDevicePoolName(XFkType value) {
        this.devicePoolName = value;
    }

    /**
     * Gets the value of the phoneButtonTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link XFkType }
     *     
     */
    public XFkType getPhoneButtonTemplate() {
        return phoneButtonTemplate;
    }

    /**
     * Sets the value of the phoneButtonTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XFkType }
     *     
     */
    public void setPhoneButtonTemplate(XFkType value) {
        this.phoneButtonTemplate = value;
    }

    /**
     * Gets the value of the preferActCodeOverAutoReg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferActCodeOverAutoReg() {
        return preferActCodeOverAutoReg;
    }

    /**
     * Sets the value of the preferActCodeOverAutoReg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferActCodeOverAutoReg(String value) {
        this.preferActCodeOverAutoReg = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
