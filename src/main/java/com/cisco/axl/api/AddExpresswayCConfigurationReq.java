
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddExpresswayCConfigurationReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddExpresswayCConfigurationReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIRequest">
 *       &lt;sequence>
 *         &lt;element name="expresswayCConfiguration" type="{http://www.cisco.com/AXL/API/12.5}XExpresswayCConfiguration"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddExpresswayCConfigurationReq", propOrder = {
    "expresswayCConfiguration"
})
public class AddExpresswayCConfigurationReq
    extends APIRequest
{

    @XmlElement(required = true)
    protected XExpresswayCConfiguration expresswayCConfiguration;

    /**
     * Gets the value of the expresswayCConfiguration property.
     * 
     * @return
     *     possible object is
     *     {@link XExpresswayCConfiguration }
     *     
     */
    public XExpresswayCConfiguration getExpresswayCConfiguration() {
        return expresswayCConfiguration;
    }

    /**
     * Sets the value of the expresswayCConfiguration property.
     * 
     * @param value
     *     allowed object is
     *     {@link XExpresswayCConfiguration }
     *     
     */
    public void setExpresswayCConfiguration(XExpresswayCConfiguration value) {
        this.expresswayCConfiguration = value;
    }

}
