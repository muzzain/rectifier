
package com.cisco.axl.api;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RCiscoCloudOnboarding complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RCiscoCloudOnboarding">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="voucherExists" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="enablePushNotifications" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="enableHttpProxy" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="httpProxyAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proxyUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proxyPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enableTrustCACertificate" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="allowAnalyticsCollection" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="enableTroubleshooting" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="alarmSendEncryptedData" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="orgId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="alarmPushIntervalSecs" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
 *         &lt;element name="alarmEncryptKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="onboardingRegistrationStatus" type="{http://www.cisco.com/AXL/API/12.5}XOnboardingRegistrationStatus" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="partnerEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orgName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerOneTimePassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="alarmSeverity" type="{http://www.cisco.com/AXL/API/12.5}XAlarmSeverity" minOccurs="0"/>
 *         &lt;element name="alarmPushNowToggle" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
 *         &lt;element name="enableGDSCommunication" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="mraActivationDomain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorStatus" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence minOccurs="0">
 *                   &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="uuid" type="{http://www.cisco.com/AXL/API/12.5}XUUID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RCiscoCloudOnboarding", propOrder = {
    "voucherExists",
    "enablePushNotifications",
    "enableHttpProxy",
    "httpProxyAddress",
    "proxyUsername",
    "proxyPassword",
    "enableTrustCACertificate",
    "allowAnalyticsCollection",
    "enableTroubleshooting",
    "alarmSendEncryptedData",
    "orgId",
    "alarmPushIntervalSecs",
    "alarmEncryptKey",
    "serviceAddress",
    "onboardingRegistrationStatus",
    "email",
    "partnerEmail",
    "orgName",
    "customerOneTimePassword",
    "alarmSeverity",
    "alarmPushNowToggle",
    "enableGDSCommunication",
    "mraActivationDomain",
    "errorStatus"
})
public class RCiscoCloudOnboarding {

    protected String voucherExists;
    protected String enablePushNotifications;
    protected String enableHttpProxy;
    protected String httpProxyAddress;
    protected String proxyUsername;
    protected String proxyPassword;
    protected String enableTrustCACertificate;
    protected String allowAnalyticsCollection;
    protected String enableTroubleshooting;
    protected String alarmSendEncryptedData;
    protected String orgId;
    protected String alarmPushIntervalSecs;
    protected String alarmEncryptKey;
    protected String serviceAddress;
    protected String onboardingRegistrationStatus;
    protected String email;
    protected String partnerEmail;
    protected String orgName;
    protected String customerOneTimePassword;
    protected String alarmSeverity;
    protected String alarmPushNowToggle;
    protected String enableGDSCommunication;
    protected String mraActivationDomain;
    protected RCiscoCloudOnboarding.ErrorStatus errorStatus;
    @XmlAttribute(name = "uuid")
    protected String uuid;

    /**
     * Gets the value of the voucherExists property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherExists() {
        return voucherExists;
    }

    /**
     * Sets the value of the voucherExists property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherExists(String value) {
        this.voucherExists = value;
    }

    /**
     * Gets the value of the enablePushNotifications property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnablePushNotifications() {
        return enablePushNotifications;
    }

    /**
     * Sets the value of the enablePushNotifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnablePushNotifications(String value) {
        this.enablePushNotifications = value;
    }

    /**
     * Gets the value of the enableHttpProxy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnableHttpProxy() {
        return enableHttpProxy;
    }

    /**
     * Sets the value of the enableHttpProxy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnableHttpProxy(String value) {
        this.enableHttpProxy = value;
    }

    /**
     * Gets the value of the httpProxyAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHttpProxyAddress() {
        return httpProxyAddress;
    }

    /**
     * Sets the value of the httpProxyAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHttpProxyAddress(String value) {
        this.httpProxyAddress = value;
    }

    /**
     * Gets the value of the proxyUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProxyUsername() {
        return proxyUsername;
    }

    /**
     * Sets the value of the proxyUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProxyUsername(String value) {
        this.proxyUsername = value;
    }

    /**
     * Gets the value of the proxyPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProxyPassword() {
        return proxyPassword;
    }

    /**
     * Sets the value of the proxyPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProxyPassword(String value) {
        this.proxyPassword = value;
    }

    /**
     * Gets the value of the enableTrustCACertificate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnableTrustCACertificate() {
        return enableTrustCACertificate;
    }

    /**
     * Sets the value of the enableTrustCACertificate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnableTrustCACertificate(String value) {
        this.enableTrustCACertificate = value;
    }

    /**
     * Gets the value of the allowAnalyticsCollection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowAnalyticsCollection() {
        return allowAnalyticsCollection;
    }

    /**
     * Sets the value of the allowAnalyticsCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowAnalyticsCollection(String value) {
        this.allowAnalyticsCollection = value;
    }

    /**
     * Gets the value of the enableTroubleshooting property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnableTroubleshooting() {
        return enableTroubleshooting;
    }

    /**
     * Sets the value of the enableTroubleshooting property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnableTroubleshooting(String value) {
        this.enableTroubleshooting = value;
    }

    /**
     * Gets the value of the alarmSendEncryptedData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlarmSendEncryptedData() {
        return alarmSendEncryptedData;
    }

    /**
     * Sets the value of the alarmSendEncryptedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlarmSendEncryptedData(String value) {
        this.alarmSendEncryptedData = value;
    }

    /**
     * Gets the value of the orgId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * Sets the value of the orgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgId(String value) {
        this.orgId = value;
    }

    /**
     * Gets the value of the alarmPushIntervalSecs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlarmPushIntervalSecs() {
        return alarmPushIntervalSecs;
    }

    /**
     * Sets the value of the alarmPushIntervalSecs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlarmPushIntervalSecs(String value) {
        this.alarmPushIntervalSecs = value;
    }

    /**
     * Gets the value of the alarmEncryptKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlarmEncryptKey() {
        return alarmEncryptKey;
    }

    /**
     * Sets the value of the alarmEncryptKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlarmEncryptKey(String value) {
        this.alarmEncryptKey = value;
    }

    /**
     * Gets the value of the serviceAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceAddress() {
        return serviceAddress;
    }

    /**
     * Sets the value of the serviceAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceAddress(String value) {
        this.serviceAddress = value;
    }

    /**
     * Gets the value of the onboardingRegistrationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnboardingRegistrationStatus() {
        return onboardingRegistrationStatus;
    }

    /**
     * Sets the value of the onboardingRegistrationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnboardingRegistrationStatus(String value) {
        this.onboardingRegistrationStatus = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the partnerEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerEmail() {
        return partnerEmail;
    }

    /**
     * Sets the value of the partnerEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerEmail(String value) {
        this.partnerEmail = value;
    }

    /**
     * Gets the value of the orgName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * Sets the value of the orgName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgName(String value) {
        this.orgName = value;
    }

    /**
     * Gets the value of the customerOneTimePassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerOneTimePassword() {
        return customerOneTimePassword;
    }

    /**
     * Sets the value of the customerOneTimePassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerOneTimePassword(String value) {
        this.customerOneTimePassword = value;
    }

    /**
     * Gets the value of the alarmSeverity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlarmSeverity() {
        return alarmSeverity;
    }

    /**
     * Sets the value of the alarmSeverity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlarmSeverity(String value) {
        this.alarmSeverity = value;
    }

    /**
     * Gets the value of the alarmPushNowToggle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlarmPushNowToggle() {
        return alarmPushNowToggle;
    }

    /**
     * Sets the value of the alarmPushNowToggle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlarmPushNowToggle(String value) {
        this.alarmPushNowToggle = value;
    }

    /**
     * Gets the value of the enableGDSCommunication property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnableGDSCommunication() {
        return enableGDSCommunication;
    }

    /**
     * Sets the value of the enableGDSCommunication property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnableGDSCommunication(String value) {
        this.enableGDSCommunication = value;
    }

    /**
     * Gets the value of the mraActivationDomain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMraActivationDomain() {
        return mraActivationDomain;
    }

    /**
     * Sets the value of the mraActivationDomain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMraActivationDomain(String value) {
        this.mraActivationDomain = value;
    }

    /**
     * Gets the value of the errorStatus property.
     * 
     * @return
     *     possible object is
     *     {@link RCiscoCloudOnboarding.ErrorStatus }
     *     
     */
    public RCiscoCloudOnboarding.ErrorStatus getErrorStatus() {
        return errorStatus;
    }

    /**
     * Sets the value of the errorStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link RCiscoCloudOnboarding.ErrorStatus }
     *     
     */
    public void setErrorStatus(RCiscoCloudOnboarding.ErrorStatus value) {
        this.errorStatus = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence minOccurs="0">
     *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "status"
    })
    public static class ErrorStatus {

        protected List<String> status;

        /**
         * Gets the value of the status property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the status property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStatus().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getStatus() {
            if (status == null) {
                status = new ArrayList<String>();
            }
            return this.status;
        }

    }

}
