
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LExpresswayCConfiguration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LExpresswayCConfiguration">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="HostNameOrIP" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.cisco.com/AXL/API/12.5}String50" minOccurs="0"/>
 *         &lt;element name="X509SubjectNameorSubjectAlternateName" type="{http://www.cisco.com/AXL/API/12.5}String4096" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="uuid" type="{http://www.cisco.com/AXL/API/12.5}XUUID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LExpresswayCConfiguration", propOrder = {
    "hostNameOrIP",
    "description",
    "x509SubjectNameorSubjectAlternateName"
})
public class LExpresswayCConfiguration {

    @XmlElement(name = "HostNameOrIP")
    protected String hostNameOrIP;
    protected String description;
    @XmlElement(name = "X509SubjectNameorSubjectAlternateName")
    protected String x509SubjectNameorSubjectAlternateName;
    @XmlAttribute(name = "uuid")
    protected String uuid;

    /**
     * Gets the value of the hostNameOrIP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostNameOrIP() {
        return hostNameOrIP;
    }

    /**
     * Sets the value of the hostNameOrIP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostNameOrIP(String value) {
        this.hostNameOrIP = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the x509SubjectNameorSubjectAlternateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getX509SubjectNameorSubjectAlternateName() {
        return x509SubjectNameorSubjectAlternateName;
    }

    /**
     * Sets the value of the x509SubjectNameorSubjectAlternateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setX509SubjectNameorSubjectAlternateName(String value) {
        this.x509SubjectNameorSubjectAlternateName = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
