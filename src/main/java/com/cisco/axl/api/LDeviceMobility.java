
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LDeviceMobility complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LDeviceMobility">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subNetDetails" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice minOccurs="0">
 *                   &lt;element name="ipv4SubNetDetails" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence minOccurs="0">
 *                             &lt;element name="ipv4Subnet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ipv4SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ipv6SubNetDetails" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence minOccurs="0">
 *                             &lt;element name="ipv6Subnet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ipv6SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="uuid" type="{http://www.cisco.com/AXL/API/12.5}XUUID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LDeviceMobility", propOrder = {
    "name",
    "subNetDetails"
})
public class LDeviceMobility {

    protected String name;
    protected LDeviceMobility.SubNetDetails subNetDetails;
    @XmlAttribute(name = "uuid")
    protected String uuid;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the subNetDetails property.
     * 
     * @return
     *     possible object is
     *     {@link LDeviceMobility.SubNetDetails }
     *     
     */
    public LDeviceMobility.SubNetDetails getSubNetDetails() {
        return subNetDetails;
    }

    /**
     * Sets the value of the subNetDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link LDeviceMobility.SubNetDetails }
     *     
     */
    public void setSubNetDetails(LDeviceMobility.SubNetDetails value) {
        this.subNetDetails = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice minOccurs="0">
     *         &lt;element name="ipv4SubNetDetails" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence minOccurs="0">
     *                   &lt;element name="ipv4Subnet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ipv4SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ipv6SubNetDetails" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence minOccurs="0">
     *                   &lt;element name="ipv6Subnet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ipv6SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ipv4SubNetDetails",
        "ipv6SubNetDetails"
    })
    public static class SubNetDetails {

        protected LDeviceMobility.SubNetDetails.Ipv4SubNetDetails ipv4SubNetDetails;
        protected LDeviceMobility.SubNetDetails.Ipv6SubNetDetails ipv6SubNetDetails;

        /**
         * Gets the value of the ipv4SubNetDetails property.
         * 
         * @return
         *     possible object is
         *     {@link LDeviceMobility.SubNetDetails.Ipv4SubNetDetails }
         *     
         */
        public LDeviceMobility.SubNetDetails.Ipv4SubNetDetails getIpv4SubNetDetails() {
            return ipv4SubNetDetails;
        }

        /**
         * Sets the value of the ipv4SubNetDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link LDeviceMobility.SubNetDetails.Ipv4SubNetDetails }
         *     
         */
        public void setIpv4SubNetDetails(LDeviceMobility.SubNetDetails.Ipv4SubNetDetails value) {
            this.ipv4SubNetDetails = value;
        }

        /**
         * Gets the value of the ipv6SubNetDetails property.
         * 
         * @return
         *     possible object is
         *     {@link LDeviceMobility.SubNetDetails.Ipv6SubNetDetails }
         *     
         */
        public LDeviceMobility.SubNetDetails.Ipv6SubNetDetails getIpv6SubNetDetails() {
            return ipv6SubNetDetails;
        }

        /**
         * Sets the value of the ipv6SubNetDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link LDeviceMobility.SubNetDetails.Ipv6SubNetDetails }
         *     
         */
        public void setIpv6SubNetDetails(LDeviceMobility.SubNetDetails.Ipv6SubNetDetails value) {
            this.ipv6SubNetDetails = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence minOccurs="0">
         *         &lt;element name="ipv4Subnet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ipv4SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ipv4Subnet",
            "ipv4SubNetMaskSz"
        })
        public static class Ipv4SubNetDetails {

            protected String ipv4Subnet;
            protected String ipv4SubNetMaskSz;

            /**
             * Gets the value of the ipv4Subnet property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpv4Subnet() {
                return ipv4Subnet;
            }

            /**
             * Sets the value of the ipv4Subnet property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpv4Subnet(String value) {
                this.ipv4Subnet = value;
            }

            /**
             * Gets the value of the ipv4SubNetMaskSz property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpv4SubNetMaskSz() {
                return ipv4SubNetMaskSz;
            }

            /**
             * Sets the value of the ipv4SubNetMaskSz property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpv4SubNetMaskSz(String value) {
                this.ipv4SubNetMaskSz = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence minOccurs="0">
         *         &lt;element name="ipv6Subnet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ipv6SubNetMaskSz" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ipv6Subnet",
            "ipv6SubNetMaskSz"
        })
        public static class Ipv6SubNetDetails {

            protected String ipv6Subnet;
            protected String ipv6SubNetMaskSz;

            /**
             * Gets the value of the ipv6Subnet property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpv6Subnet() {
                return ipv6Subnet;
            }

            /**
             * Sets the value of the ipv6Subnet property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpv6Subnet(String value) {
                this.ipv6Subnet = value;
            }

            /**
             * Gets the value of the ipv6SubNetMaskSz property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpv6SubNetMaskSz() {
                return ipv6SubNetMaskSz;
            }

            /**
             * Sets the value of the ipv6SubNetMaskSz property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpv6SubNetMaskSz(String value) {
                this.ipv6SubNetMaskSz = value;
            }

        }

    }

}
