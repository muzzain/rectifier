
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XAuthorization complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XAuthorization">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="LastCommunicationAttempt" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="NextCommunicationAttempt" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="LastCommunicationStatus" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="AuthorizationFailedReason" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="AuthorizationExpires" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="EvaluationPeriodRemaining" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XAuthorization", propOrder = {
    "status",
    "lastCommunicationAttempt",
    "nextCommunicationAttempt",
    "lastCommunicationStatus",
    "authorizationFailedReason",
    "authorizationExpires",
    "evaluationPeriodRemaining"
})
public class XAuthorization {

    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "LastCommunicationAttempt")
    protected String lastCommunicationAttempt;
    @XmlElement(name = "NextCommunicationAttempt")
    protected String nextCommunicationAttempt;
    @XmlElement(name = "LastCommunicationStatus")
    protected String lastCommunicationStatus;
    @XmlElement(name = "AuthorizationFailedReason")
    protected String authorizationFailedReason;
    @XmlElement(name = "AuthorizationExpires")
    protected String authorizationExpires;
    @XmlElement(name = "EvaluationPeriodRemaining")
    protected String evaluationPeriodRemaining;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the lastCommunicationAttempt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastCommunicationAttempt() {
        return lastCommunicationAttempt;
    }

    /**
     * Sets the value of the lastCommunicationAttempt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastCommunicationAttempt(String value) {
        this.lastCommunicationAttempt = value;
    }

    /**
     * Gets the value of the nextCommunicationAttempt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextCommunicationAttempt() {
        return nextCommunicationAttempt;
    }

    /**
     * Sets the value of the nextCommunicationAttempt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextCommunicationAttempt(String value) {
        this.nextCommunicationAttempt = value;
    }

    /**
     * Gets the value of the lastCommunicationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastCommunicationStatus() {
        return lastCommunicationStatus;
    }

    /**
     * Sets the value of the lastCommunicationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastCommunicationStatus(String value) {
        this.lastCommunicationStatus = value;
    }

    /**
     * Gets the value of the authorizationFailedReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationFailedReason() {
        return authorizationFailedReason;
    }

    /**
     * Sets the value of the authorizationFailedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationFailedReason(String value) {
        this.authorizationFailedReason = value;
    }

    /**
     * Gets the value of the authorizationExpires property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationExpires() {
        return authorizationExpires;
    }

    /**
     * Sets the value of the authorizationExpires property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationExpires(String value) {
        this.authorizationExpires = value;
    }

    /**
     * Gets the value of the evaluationPeriodRemaining property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEvaluationPeriodRemaining() {
        return evaluationPeriodRemaining;
    }

    /**
     * Sets the value of the evaluationPeriodRemaining property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEvaluationPeriodRemaining(String value) {
        this.evaluationPeriodRemaining = value;
    }

}
