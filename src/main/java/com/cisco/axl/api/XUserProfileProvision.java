
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XUserProfileProvision complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XUserProfileProvision">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="name" type="{http://www.cisco.com/AXL/API/12.5}String50"/>
 *         &lt;element name="description" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="deskPhones" type="{http://www.cisco.com/AXL/API/12.5}XFkType" minOccurs="0"/>
 *         &lt;element name="mobileDevices" type="{http://www.cisco.com/AXL/API/12.5}XFkType" minOccurs="0"/>
 *         &lt;element name="profile" type="{http://www.cisco.com/AXL/API/12.5}XFkType" minOccurs="0"/>
 *         &lt;element name="universalLineTemplate" type="{http://www.cisco.com/AXL/API/12.5}XFkType" minOccurs="0"/>
 *         &lt;element name="allowProvision" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="limitProvision" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
 *         &lt;element name="defaultUserProfile" type="{http://www.cisco.com/AXL/API/12.5}XFkType" minOccurs="0"/>
 *         &lt;element name="enableMra" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *         &lt;element name="mraPolicy_Desktop" type="{http://www.cisco.com/AXL/API/12.5}XMRAPolicy" minOccurs="0"/>
 *         &lt;element name="mraPolicy_Mobile" type="{http://www.cisco.com/AXL/API/12.5}XMRAPolicy" minOccurs="0"/>
 *         &lt;element name="allowProvisionEMMaxLoginTime" type="{http://www.cisco.com/AXL/API/12.5}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XUserProfileProvision", propOrder = {
    "name",
    "description",
    "deskPhones",
    "mobileDevices",
    "profile",
    "universalLineTemplate",
    "allowProvision",
    "limitProvision",
    "defaultUserProfile",
    "enableMra",
    "mraPolicyDesktop",
    "mraPolicyMobile",
    "allowProvisionEMMaxLoginTime"
})
public class XUserProfileProvision {

    protected String name;
    protected String description;
    protected XFkType deskPhones;
    protected XFkType mobileDevices;
    protected XFkType profile;
    protected XFkType universalLineTemplate;
    @XmlElement(defaultValue = "false")
    protected String allowProvision;
    @XmlElement(defaultValue = "10")
    protected String limitProvision;
    protected XFkType defaultUserProfile;
    @XmlElement(defaultValue = "true")
    protected String enableMra;
    @XmlElement(name = "mraPolicy_Desktop", defaultValue = "IM & Presence, Voice and Video calls")
    protected String mraPolicyDesktop;
    @XmlElement(name = "mraPolicy_Mobile", defaultValue = "IM & Presence, Voice and Video calls")
    protected String mraPolicyMobile;
    @XmlElement(defaultValue = "false")
    protected String allowProvisionEMMaxLoginTime;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the deskPhones property.
     * 
     * @return
     *     possible object is
     *     {@link XFkType }
     *     
     */
    public XFkType getDeskPhones() {
        return deskPhones;
    }

    /**
     * Sets the value of the deskPhones property.
     * 
     * @param value
     *     allowed object is
     *     {@link XFkType }
     *     
     */
    public void setDeskPhones(XFkType value) {
        this.deskPhones = value;
    }

    /**
     * Gets the value of the mobileDevices property.
     * 
     * @return
     *     possible object is
     *     {@link XFkType }
     *     
     */
    public XFkType getMobileDevices() {
        return mobileDevices;
    }

    /**
     * Sets the value of the mobileDevices property.
     * 
     * @param value
     *     allowed object is
     *     {@link XFkType }
     *     
     */
    public void setMobileDevices(XFkType value) {
        this.mobileDevices = value;
    }

    /**
     * Gets the value of the profile property.
     * 
     * @return
     *     possible object is
     *     {@link XFkType }
     *     
     */
    public XFkType getProfile() {
        return profile;
    }

    /**
     * Sets the value of the profile property.
     * 
     * @param value
     *     allowed object is
     *     {@link XFkType }
     *     
     */
    public void setProfile(XFkType value) {
        this.profile = value;
    }

    /**
     * Gets the value of the universalLineTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link XFkType }
     *     
     */
    public XFkType getUniversalLineTemplate() {
        return universalLineTemplate;
    }

    /**
     * Sets the value of the universalLineTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XFkType }
     *     
     */
    public void setUniversalLineTemplate(XFkType value) {
        this.universalLineTemplate = value;
    }

    /**
     * Gets the value of the allowProvision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowProvision() {
        return allowProvision;
    }

    /**
     * Sets the value of the allowProvision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowProvision(String value) {
        this.allowProvision = value;
    }

    /**
     * Gets the value of the limitProvision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitProvision() {
        return limitProvision;
    }

    /**
     * Sets the value of the limitProvision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitProvision(String value) {
        this.limitProvision = value;
    }

    /**
     * Gets the value of the defaultUserProfile property.
     * 
     * @return
     *     possible object is
     *     {@link XFkType }
     *     
     */
    public XFkType getDefaultUserProfile() {
        return defaultUserProfile;
    }

    /**
     * Sets the value of the defaultUserProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link XFkType }
     *     
     */
    public void setDefaultUserProfile(XFkType value) {
        this.defaultUserProfile = value;
    }

    /**
     * Gets the value of the enableMra property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnableMra() {
        return enableMra;
    }

    /**
     * Sets the value of the enableMra property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnableMra(String value) {
        this.enableMra = value;
    }

    /**
     * Gets the value of the mraPolicyDesktop property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMraPolicyDesktop() {
        return mraPolicyDesktop;
    }

    /**
     * Sets the value of the mraPolicyDesktop property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMraPolicyDesktop(String value) {
        this.mraPolicyDesktop = value;
    }

    /**
     * Gets the value of the mraPolicyMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMraPolicyMobile() {
        return mraPolicyMobile;
    }

    /**
     * Sets the value of the mraPolicyMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMraPolicyMobile(String value) {
        this.mraPolicyMobile = value;
    }

    /**
     * Gets the value of the allowProvisionEMMaxLoginTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowProvisionEMMaxLoginTime() {
        return allowProvisionEMMaxLoginTime;
    }

    /**
     * Sets the value of the allowProvisionEMMaxLoginTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowProvisionEMMaxLoginTime(String value) {
        this.allowProvisionEMMaxLoginTime = value;
    }

}
