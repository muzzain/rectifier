
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetDeviceDefaultsRes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDeviceDefaultsRes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIResponse">
 *       &lt;sequence>
 *         &lt;element name="return">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="deviceDefaults" type="{http://www.cisco.com/AXL/API/12.5}RDeviceDefaults"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDeviceDefaultsRes", propOrder = {
    "_return"
})
public class GetDeviceDefaultsRes
    extends APIResponse
{

    @XmlElement(name = "return", required = true)
    protected GetDeviceDefaultsRes.Return _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GetDeviceDefaultsRes.Return }
     *     
     */
    public GetDeviceDefaultsRes.Return getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDeviceDefaultsRes.Return }
     *     
     */
    public void setReturn(GetDeviceDefaultsRes.Return value) {
        this._return = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="deviceDefaults" type="{http://www.cisco.com/AXL/API/12.5}RDeviceDefaults"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "deviceDefaults"
    })
    public static class Return {

        @XmlElement(required = true)
        protected RDeviceDefaults deviceDefaults;

        /**
         * Gets the value of the deviceDefaults property.
         * 
         * @return
         *     possible object is
         *     {@link RDeviceDefaults }
         *     
         */
        public RDeviceDefaults getDeviceDefaults() {
            return deviceDefaults;
        }

        /**
         * Sets the value of the deviceDefaults property.
         * 
         * @param value
         *     allowed object is
         *     {@link RDeviceDefaults }
         *     
         */
        public void setDeviceDefaults(RDeviceDefaults value) {
            this.deviceDefaults = value;
        }

    }

}
