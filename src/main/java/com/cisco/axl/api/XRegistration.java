
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XRegistration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XRegistration">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="SmartAccount" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="VirtualAccount" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="LastRenewalAttempt" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="NextRenewalAttempt" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="LastRenewalStatus" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="RegistrationFailedReason" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="RegistrationExpires" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="ProductUDI" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *         &lt;element name="ExportControlFunctionality" type="{http://www.cisco.com/AXL/API/12.5}String255" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XRegistration", propOrder = {
    "status",
    "smartAccount",
    "virtualAccount",
    "lastRenewalAttempt",
    "nextRenewalAttempt",
    "lastRenewalStatus",
    "registrationFailedReason",
    "registrationExpires",
    "productUDI",
    "exportControlFunctionality"
})
public class XRegistration {

    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "SmartAccount")
    protected String smartAccount;
    @XmlElement(name = "VirtualAccount")
    protected String virtualAccount;
    @XmlElement(name = "LastRenewalAttempt")
    protected String lastRenewalAttempt;
    @XmlElement(name = "NextRenewalAttempt")
    protected String nextRenewalAttempt;
    @XmlElement(name = "LastRenewalStatus")
    protected String lastRenewalStatus;
    @XmlElement(name = "RegistrationFailedReason")
    protected String registrationFailedReason;
    @XmlElement(name = "RegistrationExpires")
    protected String registrationExpires;
    @XmlElement(name = "ProductUDI")
    protected String productUDI;
    @XmlElement(name = "ExportControlFunctionality")
    protected String exportControlFunctionality;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the smartAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmartAccount() {
        return smartAccount;
    }

    /**
     * Sets the value of the smartAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmartAccount(String value) {
        this.smartAccount = value;
    }

    /**
     * Gets the value of the virtualAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirtualAccount() {
        return virtualAccount;
    }

    /**
     * Sets the value of the virtualAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirtualAccount(String value) {
        this.virtualAccount = value;
    }

    /**
     * Gets the value of the lastRenewalAttempt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastRenewalAttempt() {
        return lastRenewalAttempt;
    }

    /**
     * Sets the value of the lastRenewalAttempt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastRenewalAttempt(String value) {
        this.lastRenewalAttempt = value;
    }

    /**
     * Gets the value of the nextRenewalAttempt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextRenewalAttempt() {
        return nextRenewalAttempt;
    }

    /**
     * Sets the value of the nextRenewalAttempt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextRenewalAttempt(String value) {
        this.nextRenewalAttempt = value;
    }

    /**
     * Gets the value of the lastRenewalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastRenewalStatus() {
        return lastRenewalStatus;
    }

    /**
     * Sets the value of the lastRenewalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastRenewalStatus(String value) {
        this.lastRenewalStatus = value;
    }

    /**
     * Gets the value of the registrationFailedReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationFailedReason() {
        return registrationFailedReason;
    }

    /**
     * Sets the value of the registrationFailedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationFailedReason(String value) {
        this.registrationFailedReason = value;
    }

    /**
     * Gets the value of the registrationExpires property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationExpires() {
        return registrationExpires;
    }

    /**
     * Sets the value of the registrationExpires property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationExpires(String value) {
        this.registrationExpires = value;
    }

    /**
     * Gets the value of the productUDI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductUDI() {
        return productUDI;
    }

    /**
     * Sets the value of the productUDI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductUDI(String value) {
        this.productUDI = value;
    }

    /**
     * Gets the value of the exportControlFunctionality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExportControlFunctionality() {
        return exportControlFunctionality;
    }

    /**
     * Sets the value of the exportControlFunctionality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExportControlFunctionality(String value) {
        this.exportControlFunctionality = value;
    }

}
