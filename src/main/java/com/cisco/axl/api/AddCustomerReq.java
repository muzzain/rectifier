
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddCustomerReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddCustomerReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIRequest">
 *       &lt;sequence>
 *         &lt;element name="customer" type="{http://www.cisco.com/AXL/API/12.5}XCustomer"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddCustomerReq", propOrder = {
    "customer"
})
public class AddCustomerReq
    extends APIRequest
{

    @XmlElement(required = true)
    protected XCustomer customer;

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link XCustomer }
     *     
     */
    public XCustomer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link XCustomer }
     *     
     */
    public void setCustomer(XCustomer value) {
        this.customer = value;
    }

}
