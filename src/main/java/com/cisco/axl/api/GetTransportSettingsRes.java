
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Returns overall transport settings details
 * 
 * <p>Java class for GetTransportSettingsRes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetTransportSettingsRes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIResponse">
 *       &lt;sequence>
 *         &lt;element name="return">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TransportSettings" type="{http://www.cisco.com/AXL/API/12.5}XTransportSettings"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTransportSettingsRes", propOrder = {
    "_return",
    "transportSettings"
})
public class GetTransportSettingsRes
    extends APIResponse
{

    @XmlElement(name = "return", required = true)
    protected String _return;
    @XmlElement(name = "TransportSettings", required = true)
    protected XTransportSettings transportSettings;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturn(String value) {
        this._return = value;
    }

    /**
     * Gets the value of the transportSettings property.
     * 
     * @return
     *     possible object is
     *     {@link XTransportSettings }
     *     
     */
    public XTransportSettings getTransportSettings() {
        return transportSettings;
    }

    /**
     * Sets the value of the transportSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link XTransportSettings }
     *     
     */
    public void setTransportSettings(XTransportSettings value) {
        this.transportSettings = value;
    }

}
