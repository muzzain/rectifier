
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Returns overall licensing status
 * 
 * <p>Java class for GetSmartLicenseStatusRes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetSmartLicenseStatusRes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIResponse">
 *       &lt;sequence>
 *         &lt;element name="return">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LicenseDetails">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SmartLicensing">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="Registration" type="{http://www.cisco.com/AXL/API/12.5}XRegistration"/>
 *                   &lt;element name="Authorization" type="{http://www.cisco.com/AXL/API/12.5}XAuthorization"/>
 *                   &lt;element name="LicenseStatus" type="{http://www.cisco.com/AXL/API/12.5}XLicenseStatus"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSmartLicenseStatusRes", propOrder = {
    "_return",
    "licenseDetails"
})
public class GetSmartLicenseStatusRes
    extends APIResponse
{

    @XmlElement(name = "return", required = true)
    protected String _return;
    @XmlElement(name = "LicenseDetails", required = true)
    protected GetSmartLicenseStatusRes.LicenseDetails licenseDetails;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturn(String value) {
        this._return = value;
    }

    /**
     * Gets the value of the licenseDetails property.
     * 
     * @return
     *     possible object is
     *     {@link GetSmartLicenseStatusRes.LicenseDetails }
     *     
     */
    public GetSmartLicenseStatusRes.LicenseDetails getLicenseDetails() {
        return licenseDetails;
    }

    /**
     * Sets the value of the licenseDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSmartLicenseStatusRes.LicenseDetails }
     *     
     */
    public void setLicenseDetails(GetSmartLicenseStatusRes.LicenseDetails value) {
        this.licenseDetails = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SmartLicensing">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="Registration" type="{http://www.cisco.com/AXL/API/12.5}XRegistration"/>
     *         &lt;element name="Authorization" type="{http://www.cisco.com/AXL/API/12.5}XAuthorization"/>
     *         &lt;element name="LicenseStatus" type="{http://www.cisco.com/AXL/API/12.5}XLicenseStatus"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "smartLicensing",
        "registration",
        "authorization",
        "licenseStatus"
    })
    public static class LicenseDetails {

        @XmlElement(name = "SmartLicensing", required = true)
        protected String smartLicensing;
        @XmlElement(name = "Registration", required = true)
        protected XRegistration registration;
        @XmlElement(name = "Authorization", required = true)
        protected XAuthorization authorization;
        @XmlElement(name = "LicenseStatus", required = true)
        protected XLicenseStatus licenseStatus;

        /**
         * Gets the value of the smartLicensing property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSmartLicensing() {
            return smartLicensing;
        }

        /**
         * Sets the value of the smartLicensing property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSmartLicensing(String value) {
            this.smartLicensing = value;
        }

        /**
         * Gets the value of the registration property.
         * 
         * @return
         *     possible object is
         *     {@link XRegistration }
         *     
         */
        public XRegistration getRegistration() {
            return registration;
        }

        /**
         * Sets the value of the registration property.
         * 
         * @param value
         *     allowed object is
         *     {@link XRegistration }
         *     
         */
        public void setRegistration(XRegistration value) {
            this.registration = value;
        }

        /**
         * Gets the value of the authorization property.
         * 
         * @return
         *     possible object is
         *     {@link XAuthorization }
         *     
         */
        public XAuthorization getAuthorization() {
            return authorization;
        }

        /**
         * Sets the value of the authorization property.
         * 
         * @param value
         *     allowed object is
         *     {@link XAuthorization }
         *     
         */
        public void setAuthorization(XAuthorization value) {
            this.authorization = value;
        }

        /**
         * Gets the value of the licenseStatus property.
         * 
         * @return
         *     possible object is
         *     {@link XLicenseStatus }
         *     
         */
        public XLicenseStatus getLicenseStatus() {
            return licenseStatus;
        }

        /**
         * Sets the value of the licenseStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link XLicenseStatus }
         *     
         */
        public void setLicenseStatus(XLicenseStatus value) {
            this.licenseStatus = value;
        }

    }

}
