
package com.cisco.axl.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XDeviceDefaults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XDeviceDefaults">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="LoadInformation" type="{http://www.cisco.com/AXL/API/12.5}XLoadInformation"/>
 *         &lt;element name="InactiveLoadInformation" type="{http://www.cisco.com/AXL/API/12.5}XLoadInformation"/>
 *         &lt;element name="DevicePoolName" type="{http://www.cisco.com/AXL/API/12.5}XFkType"/>
 *         &lt;element name="PhoneButtonTemplate" type="{http://www.cisco.com/AXL/API/12.5}XFkType"/>
 *         &lt;element name="VersionStamp" type="{http://www.cisco.com/AXL/API/12.5}String128"/>
 *         &lt;element name="PreferActCodeOverAutoReg" type="{http://www.cisco.com/AXL/API/12.5}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XDeviceDefaults", propOrder = {
    "loadInformation",
    "inactiveLoadInformation",
    "devicePoolName",
    "phoneButtonTemplate",
    "versionStamp",
    "preferActCodeOverAutoReg"
})
public class XDeviceDefaults {

    @XmlElement(name = "LoadInformation")
    protected XLoadInformation loadInformation;
    @XmlElement(name = "InactiveLoadInformation")
    protected XLoadInformation inactiveLoadInformation;
    @XmlElementRef(name = "DevicePoolName", type = JAXBElement.class, required = false)
    protected JAXBElement<XFkType> devicePoolName;
    @XmlElementRef(name = "PhoneButtonTemplate", type = JAXBElement.class, required = false)
    protected JAXBElement<XFkType> phoneButtonTemplate;
    @XmlElementRef(name = "VersionStamp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> versionStamp;
    @XmlElement(name = "PreferActCodeOverAutoReg")
    protected String preferActCodeOverAutoReg;

    /**
     * Gets the value of the loadInformation property.
     * 
     * @return
     *     possible object is
     *     {@link XLoadInformation }
     *     
     */
    public XLoadInformation getLoadInformation() {
        return loadInformation;
    }

    /**
     * Sets the value of the loadInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link XLoadInformation }
     *     
     */
    public void setLoadInformation(XLoadInformation value) {
        this.loadInformation = value;
    }

    /**
     * Gets the value of the inactiveLoadInformation property.
     * 
     * @return
     *     possible object is
     *     {@link XLoadInformation }
     *     
     */
    public XLoadInformation getInactiveLoadInformation() {
        return inactiveLoadInformation;
    }

    /**
     * Sets the value of the inactiveLoadInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link XLoadInformation }
     *     
     */
    public void setInactiveLoadInformation(XLoadInformation value) {
        this.inactiveLoadInformation = value;
    }

    /**
     * Gets the value of the devicePoolName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XFkType }{@code >}
     *     
     */
    public JAXBElement<XFkType> getDevicePoolName() {
        return devicePoolName;
    }

    /**
     * Sets the value of the devicePoolName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XFkType }{@code >}
     *     
     */
    public void setDevicePoolName(JAXBElement<XFkType> value) {
        this.devicePoolName = value;
    }

    /**
     * Gets the value of the phoneButtonTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XFkType }{@code >}
     *     
     */
    public JAXBElement<XFkType> getPhoneButtonTemplate() {
        return phoneButtonTemplate;
    }

    /**
     * Sets the value of the phoneButtonTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XFkType }{@code >}
     *     
     */
    public void setPhoneButtonTemplate(JAXBElement<XFkType> value) {
        this.phoneButtonTemplate = value;
    }

    /**
     * Gets the value of the versionStamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVersionStamp() {
        return versionStamp;
    }

    /**
     * Sets the value of the versionStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVersionStamp(JAXBElement<String> value) {
        this.versionStamp = value;
    }

    /**
     * Gets the value of the preferActCodeOverAutoReg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferActCodeOverAutoReg() {
        return preferActCodeOverAutoReg;
    }

    /**
     * Sets the value of the preferActCodeOverAutoReg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferActCodeOverAutoReg(String value) {
        this.preferActCodeOverAutoReg = value;
    }

}
