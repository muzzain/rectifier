
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddPhoneActivationCodeReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddPhoneActivationCodeReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.cisco.com/AXL/API/12.5}APIRequest">
 *       &lt;sequence>
 *         &lt;element name="phoneActivationCode" type="{http://www.cisco.com/AXL/API/12.5}XPhoneActivationCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddPhoneActivationCodeReq", propOrder = {
    "phoneActivationCode"
})
public class AddPhoneActivationCodeReq
    extends APIRequest
{

    @XmlElement(required = true)
    protected XPhoneActivationCode phoneActivationCode;

    /**
     * Gets the value of the phoneActivationCode property.
     * 
     * @return
     *     possible object is
     *     {@link XPhoneActivationCode }
     *     
     */
    public XPhoneActivationCode getPhoneActivationCode() {
        return phoneActivationCode;
    }

    /**
     * Sets the value of the phoneActivationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link XPhoneActivationCode }
     *     
     */
    public void setPhoneActivationCode(XPhoneActivationCode value) {
        this.phoneActivationCode = value;
    }

}
