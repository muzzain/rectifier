
package com.cisco.axl.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XPhoneActivationCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XPhoneActivationCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="activationCodeExpiry" type="{http://www.cisco.com/AXL/API/12.5}XInteger" minOccurs="0"/>
 *         &lt;element name="phoneName" type="{http://www.cisco.com/AXL/API/12.5}String255"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XPhoneActivationCode", propOrder = {
    "activationCodeExpiry",
    "phoneName"
})
public class XPhoneActivationCode {

    protected String activationCodeExpiry;
    protected String phoneName;

    /**
     * Gets the value of the activationCodeExpiry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationCodeExpiry() {
        return activationCodeExpiry;
    }

    /**
     * Sets the value of the activationCodeExpiry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationCodeExpiry(String value) {
        this.activationCodeExpiry = value;
    }

    /**
     * Gets the value of the phoneName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneName() {
        return phoneName;
    }

    /**
     * Sets the value of the phoneName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneName(String value) {
        this.phoneName = value;
    }

}
