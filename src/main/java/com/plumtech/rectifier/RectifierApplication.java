package com.plumtech.rectifier;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.cisco.axl.api.AXLAPIService;
import com.cisco.axl.api.AXLPort;

@SpringBootApplication
public class RectifierApplication {
	private static Logger LOG = LoggerFactory.getLogger(RectifierApplication.class);
	static {
		disableSslVerification();
	}

	public static void main(String[] args) throws Exception {
		LOG.debug("Start Executing Application");
		SpringApplication.run(RectifierApplication.class, args);

	}

	@Value("${axl.endpoint}")
	private String endpoint;

	@Value("${axl.user}")
	private String user;

	@Value("${axl.password}")
	private String password;

	@Bean
	public AXLPort getAXLClient() {
		AXLAPIService axlService = new AXLAPIService();
		AXLPort axlPort = axlService.getAXLPort();

		((BindingProvider) axlPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, this.endpoint);
		((BindingProvider) axlPort).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, this.user);
		((BindingProvider) axlPort).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, this.password);
		return axlPort;
	}

	private static void disableSslVerification() {
		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
}
