package com.plumtech.rectifier.rule;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cisco.axl.api.XLdapAuthentication;
import com.cisco.axl.api.XLdapAuthentication.Servers.Server;
import com.plumtech.rectifier.model.RemediationInput;
import com.plumtech.rectifier.model.RemediationResult;
import com.plumtech.rectifier.model.ValidationResult;
import com.plumtech.rectifier.services.communication.LDAPService;

@Component
public class LDAPAuthenticationRule implements Validateable {

	@Autowired
	private LDAPService ldapService;

	@Override
	public List<ValidationResult> validate() throws Exception {
		List<ValidationResult> results = new ArrayList<>();
		ValidationResult result = new ValidationResult();
		result.setName("LDAP Authentication");
		results.add(result);
		XLdapAuthentication ldapAuthentication = null;
		try {
			ldapAuthentication = ldapService.getLDAPAuthentication();
			// 2) Check if “Use LDAP Authentication for End Users” box is checked
		} catch (SOAPFaultException e) {
			SOAPFault fault = e.getFault();
			String faultString = fault.getFaultString();
			if (faultString.contains("The specified Ldap Authentication was not found")) {
				result.setResult("Users are locally Authenticated not using LDAP");
				result.setImpact("Lack of Centralized Access Control and effective User Access Management ");
				result.setRecommendation("Use LDAP for User Access");
				return results;
			}

		}

		// TODO : Assuming if XLdapAuthentication from response is null Then checkbox is
		// not checked

		String distinguishedName = ldapAuthentication.getDistinguishedName();

		String ldapPassword = ldapAuthentication.getLdapPassword();
		String userSearchBase = ldapAuthentication.getUserSearchBase();
		List<Server> servers = ldapAuthentication.getServers().getServer();
		if (distinguishedName != null && ldapPassword != null && userSearchBase != null && servers.size() > 0) {
			// TODO : Getting only First Server
			Server server = servers.get(0);
			String ldapPortNumber = server.getLdapPortNumber();
			boolean sslEnabled = parseBoolean(server.getSslEnabled());
			if ((ldapPortNumber.equals("636") || ldapPortNumber.equals("3269") && sslEnabled)) {
				result.setResult("LDAP Authentication is Secured");
				result.setImpact(null);
				result.setRecommendation(null);
				return results;
			} else if (!ldapPortNumber.equals("636") || !ldapPortNumber.equals("3269") && !sslEnabled) {
				result.setResult("LDAP Authentication is not secured");
				result.setImpact("Lack of secured User Access & Authentication Management");
				result.setRecommendation("Use secure LDAP for User Access with TLS enabled");
				result.setEnableRemediation(true);
				return results;
			}

		} else {
			result.setResult("Users are locally Authenticated not using LDAP");
			result.setImpact("Lack of Centralized Access Control and effective User Access Management ");
			result.setRecommendation("Use LDAP for User Access");
			result.setEnableRemediation(true);

		}

		return results;
	}

	public RemediationResult remediation(RemediationInput input) throws Exception {
		RemediationResult result = new RemediationResult();
		result.setResult("request not processed Successfully");
		XLdapAuthentication ldapAuthentication = ldapService.getLDAPAuthentication();
		Server server = ldapAuthentication.getServers().getServer().get(0);
		server.setLdapPortNumber(input.getInputValue());
		server.setSslEnabled("true");
		ldapService.updateLDAPAuthentication(ldapAuthentication);
		result.setResult("Successfully update LDAPAuthentication.");
		return result;
	}

	private boolean parseBoolean(String bool) {
//		<xsd:simpleType name="boolean">
//		  <xsd:restriction base="xsd:string">
//		    <xsd:pattern value="(t)|(f)|(true)|(false)|(0)|(1)"/>
//		  </xsd:restriction>
//		</xsd:simpleType>
		if (bool.equalsIgnoreCase("true") || bool.charAt(0) == 't' || bool.charAt(0) == '1') {
			return true;
		} else {
			return false;
		}
	}

}
