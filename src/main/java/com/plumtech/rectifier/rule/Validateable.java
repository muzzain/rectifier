package com.plumtech.rectifier.rule;

import java.util.List;

import com.plumtech.rectifier.model.ValidationResult;

public interface Validateable {

	public List<ValidationResult> validate() throws Exception;
}
