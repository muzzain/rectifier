package com.plumtech.rectifier.rule;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.plumtech.rectifier.model.ValidationResult;
import com.plumtech.rectifier.services.communication.ConfigService;

/*
 * 
 * For Now Using Hard coded Result , impact and Recommendation *(Not Recommended for production)  
 */
@Component
public class EnterpriseParameterRule implements Validateable {

	@Autowired
	ConfigService configService;

	@Override
	public List<ValidationResult> validate() throws Exception {
		List<ValidationResult> results = new ArrayList<>();
		Map<String, String> allEnterpriseServiceParameters = configService.getAllEnterpriseServiceParameters();

		ValidationResult clusterSecurityModeResult = validateClusterSecurityMode(allEnterpriseServiceParameters);
		results.add(clusterSecurityModeResult);

		ValidationResult validateClusterSIPOAuthMode = validateClusterSIPOAuthMode(allEnterpriseServiceParameters);
		results.add(validateClusterSIPOAuthMode);

		ValidationResult validateLBMSecurityMode = validateLBMSecurityMode(allEnterpriseServiceParameters);
		results.add(validateLBMSecurityMode);

		ValidationResult validateTFTPFileSignatureAlgorithm = validateTFTPFileSignatureAlgorithm(
				allEnterpriseServiceParameters);
		results.add(validateTFTPFileSignatureAlgorithm);

		ValidationResult validateAuthMethodForBrowserAccess = validateAuthMethodForBrowserAccess(
				allEnterpriseServiceParameters);
		results.add(validateAuthMethodForBrowserAccess);

		ValidationResult validateTerminateUserSession = validateTerminateUserSession(allEnterpriseServiceParameters);
		results.add(validateTerminateUserSession);

		ValidationResult validateCertificateValidityCheck = validateCertificateValidityCheck(
				allEnterpriseServiceParameters);
		results.add(validateCertificateValidityCheck);

		ValidationResult validateRestrictOnUserGroupOverlap = validateRestrictOnUserGroupOverlap(
				allEnterpriseServiceParameters);
		results.add(validateRestrictOnUserGroupOverlap);

		ValidationResult validateDoSProtectionFlag = validateDoSProtectionFlag(allEnterpriseServiceParameters);
		results.add(validateDoSProtectionFlag);

		return results;
	}

	private ValidationResult validateClusterSecurityMode(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("Security Parameters");
		String clusterSecurityModeValue = enterpriseParameters.get("ClusterSecurityMode");
		if (clusterSecurityModeValue != null) {
			if (clusterSecurityModeValue.equals("0")) {
				result.setResult("Cluster is in Non-Secure mode");
				result.setImpact("End Point Encryption not supported in Non-Secured Cluster mode");
				result.setRecommendation("Use External CA signed certificates to convert cluster into mixed mode");
			} else {
				result.setResult("Cluster is in Mixed/Secure mode");
			}
		}
		return result;
	}

	private ValidationResult validateClusterSIPOAuthMode(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("Security Parameters");
		String clusterSIPOAuthMode = enterpriseParameters.get("ClusterSIPOAuthMode");
		if (clusterSIPOAuthMode != null) {
			if (clusterSIPOAuthMode.equals("0")) {
				result.setResult("OAuth Token Refresh/Validation is Disabled");
				result.setImpact("Jabber device will not support secure signaling and media without CAPF");
				result.setRecommendation("Enable this feature following the pre-requisite tasks");
			} else {
				result.setResult("Cluster SIPOAuth Mode is Enabled");
			}
		}
		return result;
	}

	private ValidationResult validateLBMSecurityMode(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("Security Parameters");
		String lBMSecurityMode = enterpriseParameters.get("LBMSecurityMode");
		if (lBMSecurityMode != null) {
			if (lBMSecurityMode.equals("0")) {
				result.setResult("Effective Path Selection is Non-Secure");
				result.setImpact("Enhanced Location based CAC is not supported in Non-Secure mode");
				result.setRecommendation(
						"Use Mixed mode while transitioning from Non-Secure to Secure then change to Secure for effective and secured location-based CAC");
			} else {
				result.setResult("LBM Security Mode is enabled");
			}
		}
		return result;
	}

	private ValidationResult validateTFTPFileSignatureAlgorithm(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("Security Parameters");
		String tFTPFileSignatureAlgorithm = enterpriseParameters.get("TFTPFileSignatureAlgorithm");
		if (tFTPFileSignatureAlgorithm != null) {
			if (tFTPFileSignatureAlgorithm.equals("1")) {
				result.setResult("vulnerable hashing algorithm");
				result.setImpact("Potential of configuration and software/firmware files integrity compromise");
				result.setRecommendation("Enable SHA-2 where available and supported");
			} else {
				result.setResult("Secure hashing algorithm is enabled");
			}
		}
		return result;
	}

	private ValidationResult validateAuthMethodForBrowserAccess(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("Security Parameters");
		/*
		 * Form-based = 0 Basic = 1
		 */
		String authMethodForBrowserAccess = enterpriseParameters.get("AuthMethodForBrowserAccess");

		if (authMethodForBrowserAccess != null) {
			if (authMethodForBrowserAccess.equals("1")) {
				result.setResult("Less secure API browser access");
				result.setImpact("Potential of compromised access to APIs and backend database");
				result.setRecommendation("Implement Form based Authentication where possible");
			} else {
				result.setResult("More secure API Browser access is enabled");
			}
		}
		return result;
	}

	private ValidationResult validateTerminateUserSession(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("Security Parameters");
		/*
		 * Disabled = 0 Enabled = 1
		 */
		String terminateUserSession = enterpriseParameters.get("TerminateUserSession");
		if (terminateUserSession != null) {
			if (terminateUserSession.equals("0")) {
				result.setResult("Non-Secure User Session Management");
				result.setImpact("Potential of compromised Access Control and Principle of Least Privilege");
				result.setRecommendation("Make sure this feature is set to Enabled (as per default value)");
			} else {
				result.setResult("Secured User Session Management");
			}
		}
		return result;
	}

	private ValidationResult validateCertificateValidityCheck(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("Certificate Revocation and Expiry");
		/*
		 * Disabled = 0 Enabled = 1
		 */
		String certificateValidityCheck = enterpriseParameters.get("CertificateValidityCheck");
		if (certificateValidityCheck != null) {
			if (certificateValidityCheck.equals("0")) {
				result.setResult("Certificate Expiry/Validity Check is Disabled");
				result.setImpact(
						"Risk of Certificate expiry without notice and potential impact of some features and functionalities based on respective certificate types");
				result.setRecommendation("Enable “Certificate Validity Check” ");
			} else {
				result.setResult("Certificate Expiry/Validity Check is Enabled");
			}
		}
		return result;
	}

	private ValidationResult validateRestrictOnUserGroupOverlap(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("User Management Parameters");
		/*
		 * Minimum = 0 Maximum = 1
		 */
		String restrictOnUserGroupOverlap = enterpriseParameters.get("restrictOnUserGroupOverlap");
		if (restrictOnUserGroupOverlap != null) {
			if (restrictOnUserGroupOverlap.equals("1")) {
				result.setResult("Maximum Privilege Assigned to Users Subscribed to Multiple groups");
				result.setImpact(
						"Potential privilege creep and compliance risk due to missing Principle of least Privilege");
				result.setRecommendation("Thoroughly review the User Accounts and adjust this setting accordingly");
			} else {
				result.setResult("Minimum Privilege Assigned to Users Subscribed to Multiple groups");
			}
		}
		return result;
	}

	private ValidationResult validateDoSProtectionFlag(Map<String, String> enterpriseParameters) {
		ValidationResult result = new ValidationResult();
		result.setName("Denial-of-Service Protection");
		/*
		 * T = true
		 * F = false
		 */
		String doSProtectionFlag = enterpriseParameters.get("DoSProtectionFlag");
		if (doSProtectionFlag != null) {
			if (doSProtectionFlag.equals("F")) {
				result.setResult("Denial-of-Service Protection is disabled");
				result.setImpact("This enables protection used to thwart certain Denial-of-Service attacks");
				result.setRecommendation("Make sure this feature is set to “True” (as per default value)");
			} else {
				result.setResult("Denial-of-Service Protection is enabled");
			}
		}
		return result;
	}
}
