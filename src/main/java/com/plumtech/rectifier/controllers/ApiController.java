package com.plumtech.rectifier.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.plumtech.rectifier.model.RemediationInput;
import com.plumtech.rectifier.model.RemediationResult;
import com.plumtech.rectifier.model.ValidationResult;
import com.plumtech.rectifier.rule.LDAPAuthenticationRule;
import com.plumtech.rectifier.rule.Validateable;

@RestController
public class ApiController {
	private static Logger LOG = LoggerFactory.getLogger(ApiController.class);

	@Autowired
	private List<Validateable> validations;
	
	@Autowired
	private LDAPAuthenticationRule ldapAuthenticationRule;

	@PostMapping("/executeSuit")
	public List<ValidationResult> executeSuit() throws Exception {
		LOG.debug("Total Configured Validation Beans  = " + validations.size());
		List<ValidationResult> allResults = new ArrayList<>();
		for (Validateable validateable : validations) {
			List<ValidationResult> result = validateable.validate();
			allResults.addAll(result);
		}
		return allResults;
	}

	@PostMapping("/ldapRemediation")
	public RemediationResult ldapRemediation(RemediationInput input) throws Exception {
		RemediationResult result = ldapAuthenticationRule.remediation(input);
		return result;
	}

}
