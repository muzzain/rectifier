package com.plumtech.rectifier.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	@Value("${axl.endpoint}")
	private String endpoint;

	@Value("${axl.user}")
	private String user;

	@Value("${axl.password}")
	private String password;

	@RequestMapping("/")
	public String welcome(ModelMap map) {
		map.put("endpoint", endpoint);
		map.put("user", user);
		return "welcome";
	}

}
