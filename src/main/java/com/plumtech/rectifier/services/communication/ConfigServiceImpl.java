package com.plumtech.rectifier.services.communication;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cisco.axl.api.AXLPort;
import com.cisco.axl.api.LServiceParameter;
import com.cisco.axl.api.ListServiceParameterReq;
import com.cisco.axl.api.ListServiceParameterReq.SearchCriteria;
import com.cisco.axl.api.ListServiceParameterRes;

@Service
public class ConfigServiceImpl extends AbstractCommunicationService implements ConfigService {

	public Map<String, String> getAllEnterpriseServiceParameters() throws Exception {
		ListServiceParameterReq listServiceParameterReq = new ListServiceParameterReq();
		AXLPort axlClient = this.getAXLClient();
		LServiceParameter returnTag = new LServiceParameter();
		returnTag.setName("");
		returnTag.setValue("");

		listServiceParameterReq.setReturnedTags(returnTag);
		SearchCriteria criteria = new SearchCriteria();
		criteria.setProcessNodeName("%");
		listServiceParameterReq.setSearchCriteria(criteria);

		ListServiceParameterRes listServiceParameter = axlClient.listServiceParameter(listServiceParameterReq);
		List<LServiceParameter> serviceParameter = listServiceParameter.getReturn().getServiceParameter();
		Map<String, String> serviceParametersMap = getServiceParameterMapFromServicePAramList(serviceParameter);
		return serviceParametersMap;

	}

	private Map<String, String> getServiceParameterMapFromServicePAramList(List<LServiceParameter> serviceParameter) {
		Map<String, String> serviceParameterMap = new HashMap<String, String>();
		for (LServiceParameter lServiceParameter : serviceParameter) {
			serviceParameterMap.put(lServiceParameter.getName(), lServiceParameter.getValue());

		}
		return serviceParameterMap;
	}
}
