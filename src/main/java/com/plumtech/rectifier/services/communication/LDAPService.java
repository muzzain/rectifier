package com.plumtech.rectifier.services.communication;

import com.cisco.axl.api.XLdapAuthentication;

public interface LDAPService {
	public XLdapAuthentication getLDAPAuthentication() throws Exception;

	public String updateLDAPAuthentication(XLdapAuthentication input) throws Exception;
}
