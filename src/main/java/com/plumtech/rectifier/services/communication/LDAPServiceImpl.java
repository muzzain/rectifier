package com.plumtech.rectifier.services.communication;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cisco.axl.api.AXLPort;
import com.cisco.axl.api.GetLdapAuthenticationReq;
import com.cisco.axl.api.GetLdapAuthenticationRes;
import com.cisco.axl.api.GetLdapAuthenticationRes.Return;
import com.cisco.axl.api.StandardResponse;
import com.cisco.axl.api.UpdateLdapAuthenticationReq;
import com.cisco.axl.api.UpdateLdapAuthenticationReq.Servers;
import com.cisco.axl.api.XLdapAuthentication;
import com.cisco.axl.api.XLdapAuthentication.Servers.Server;

@Service
public class LDAPServiceImpl extends AbstractCommunicationService implements LDAPService {

	public XLdapAuthentication getLDAPAuthentication() throws Exception {

		AXLPort axlClient = this.getAXLClient();
		GetLdapAuthenticationReq axlParams = new GetLdapAuthenticationReq();
		GetLdapAuthenticationRes ldapAuthentication = axlClient.getLdapAuthentication(axlParams);
		Return resReturn = ldapAuthentication.getReturn();
		return resReturn.getLdapAuthentication();

	}

	@Override
	public String updateLDAPAuthentication(XLdapAuthentication input) throws Exception {
		AXLPort axlClient = this.getAXLClient();
		UpdateLdapAuthenticationReq axlParams = new UpdateLdapAuthenticationReq();
		axlParams.setAuthenticateEndUsers(input.getAuthenticateEndUsers());
		axlParams.setDistinguishedName(input.getDistinguishedName());
		axlParams.setLdapPassword(input.getLdapPassword());
		getServerForUpdateRequest(input.getServers(), axlParams);
		StandardResponse updateLdapAuthentication = axlClient.updateLdapAuthentication(axlParams);
		String responseMessage = updateLdapAuthentication.getReturn();
		return responseMessage;
	}

	private void getServerForUpdateRequest(XLdapAuthentication.Servers servers,
			UpdateLdapAuthenticationReq updateRequest) {
		List<Server> server = servers.getServer();
		Servers updateServers = new Servers();

		for (Server inputServer : server) {

			com.cisco.axl.api.UpdateLdapAuthenticationReq.Servers.Server srvr = new com.cisco.axl.api.UpdateLdapAuthenticationReq.Servers.Server();
			srvr.setHostName(inputServer.getHostName());
			srvr.setLdapPortNumber(inputServer.getLdapPortNumber());
			srvr.setSslEnabled(inputServer.getSslEnabled());
			updateServers.getServer().add(srvr);
		}
		updateRequest.setServers(updateServers);

	}
}
