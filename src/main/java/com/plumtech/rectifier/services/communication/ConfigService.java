package com.plumtech.rectifier.services.communication;

import java.util.Map;

public interface ConfigService {

	public Map<String, String> getAllEnterpriseServiceParameters() throws Exception;
}
