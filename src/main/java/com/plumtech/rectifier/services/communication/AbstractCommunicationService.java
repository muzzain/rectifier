package com.plumtech.rectifier.services.communication;

import org.springframework.beans.factory.annotation.Autowired;

import com.cisco.axl.api.AXLPort;

public abstract class AbstractCommunicationService {

	@Autowired
	private AXLPort axlClient;

	protected AXLPort getAXLClient() {
		return axlClient;
	}

}
