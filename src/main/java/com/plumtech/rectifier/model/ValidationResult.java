package com.plumtech.rectifier.model;

public class ValidationResult {
	private String name;
	private String result;
	private String impact;
	private String recommendation;
	private boolean enableRemediation;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnableRemediation() {
		return enableRemediation;
	}

	public void setEnableRemediation(boolean enableRemediation) {
		this.enableRemediation = enableRemediation;
	}

	@Override
	public String toString() {
		return "ValidationResult [name=" + name + ", result=" + result + ", impact=" + impact + ", recommendation="
				+ recommendation + ", enableRemediation=" + enableRemediation + "]";
	}
	
	

}
