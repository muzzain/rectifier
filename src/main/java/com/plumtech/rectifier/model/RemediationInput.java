package com.plumtech.rectifier.model;

public class RemediationInput {
	
	private String inputValue;

	public String getInputValue() {
		return inputValue;
	}

	public void setInputValue(String inputValue) {
		this.inputValue = inputValue;
	}
	

}
